import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweater_flutter/core/payment/app_payment.dart';
import 'package:sweater_flutter/core/payment/tap_payment.dart';
import 'package:sweater_flutter/core/sharedpreferences/shared_preferences_util.dart';
import 'package:sweater_flutter/core/sharedpreferences/shared_preferences_util_impl.dart';
import 'package:sweater_flutter/data/network/internet_connection_state.dart';
import 'package:sweater_flutter/data/network/dio_client.dart';
import 'package:sweater_flutter/data/network/sweater_network.dart';
import 'package:sweater_flutter/data/repositories/auth_repository_impl.dart';
import 'package:sweater_flutter/data/repositories/orders_repository_impl.dart';
import 'package:sweater_flutter/domain/repositories/auth_repository.dart';
import 'package:sweater_flutter/domain/repositories/orders_repository.dart';
import 'package:sweater_flutter/domain/usecases/create_payment_order_usecase.dart';
import 'package:sweater_flutter/domain/usecases/signin_usecase.dart';

final locator = GetIt.instance;

Future<void> configureDependencies() async {
  locator.registerLazySingleton<InternetConnectionState>(() => InternetConnectionState());
  locator.registerSingletonAsync<SharedPreferences>(() async => await SharedPreferences.getInstance());

  final sharedPreferences = await locator.getAsync<SharedPreferences>();
  locator.registerLazySingleton<SharedPreferencesUtil>(() => SharedPreferencesUtilImpl(sharedPreferences));

  // Network
  final dio = buildDioClient(locator());
  locator.registerLazySingleton<SweaterNetwork>(() => SweaterNetwork(dio));

  // Repositories
  locator.registerLazySingleton<AuthRepository>(
          () => AuthRepositoryImpl(locator()));
  locator.registerLazySingleton<OrdersRepository>(
          () => OrdersRepositoryImpl(locator()));

  // UseCases
  locator.registerLazySingleton<SignInUseCase>(() => SignInUseCase(locator()));
  locator.registerLazySingleton<CreatePaymentOrderUseCase>(() => CreatePaymentOrderUseCase(locator()));

  // Payment Gateways
  locator.registerLazySingleton<TapPayment>(() => TapPayment(locator()));
  locator.registerFactory<AppPayment>(() => AppPaymentImpl(locator(), locator()));
}