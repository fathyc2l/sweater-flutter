
import 'package:dartz/dartz.dart';
import 'package:sweater_flutter/core/base/base_repository.dart';
import 'package:sweater_flutter/core/base/base_request_body.dart';
import 'package:sweater_flutter/core/errors/app_error.dart';
import 'package:sweater_flutter/core/payment/payment_order.dart';

abstract class OrdersRepository extends BaseRepository {
  Future<Either<AppError, PaymentOrder>> createPaymentOrder({required BaseRequestBody body});
}