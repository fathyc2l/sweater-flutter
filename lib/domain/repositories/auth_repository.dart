
import 'package:sweater_flutter/core/base/base_repository.dart';
import 'package:sweater_flutter/core/base/base_request_body.dart';
import 'package:sweater_flutter/core/errors/app_error.dart';
import 'package:dartz/dartz.dart';

abstract class AuthRepository extends BaseRepository {
  Future<Either<AppError, bool>> signIn({required BaseRequestBody body});
}