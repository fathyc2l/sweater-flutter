
import 'package:dartz/dartz.dart';
import 'package:sweater_flutter/core/base/base_request_body.dart';
import 'package:sweater_flutter/core/base/base_usecase.dart';
import 'package:sweater_flutter/core/errors/app_error.dart';
import 'package:sweater_flutter/domain/repositories/auth_repository.dart';

class SignInUseCase implements BaseUseCae<Either<AppError, bool>> {
  final AuthRepository _repository;

  SignInUseCase(this._repository);

  @override
  Future<Either<AppError, bool>> call({required BaseRequestBody body}) =>
      _repository.signIn(body: body);
}