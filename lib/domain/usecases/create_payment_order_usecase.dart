
import 'package:dartz/dartz.dart';
import 'package:sweater_flutter/core/base/base_request_body.dart';
import 'package:sweater_flutter/core/base/base_usecase.dart';
import 'package:sweater_flutter/core/errors/app_error.dart';
import 'package:sweater_flutter/core/payment/payment_order.dart';
import 'package:sweater_flutter/domain/repositories/orders_repository.dart';

class CreatePaymentOrderUseCase implements BaseUseCae<Either<AppError, PaymentOrder>> {
  final OrdersRepository _ordersRepository;

  CreatePaymentOrderUseCase(this._ordersRepository);

  @override
  Future<Either<AppError, PaymentOrder>> call({required BaseRequestBody body}) =>
      _ordersRepository.createPaymentOrder(body: body);
}