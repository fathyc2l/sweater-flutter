import 'gender.dart';

class Client {
  int id;
  int countryId;
  int? numberOfBookings;
  String fullName;
  String image;
  String email;
  String mobile; // ex 0500000031
  String fullMobile; // ex 00966500000031
  String countryCode; // ex +966
  String registrationDate;
  String? zone;
  String? area;
  Gender gender;
  double rating;
  double walletAmount; // the current amount saved in client's wallet
  bool isVerified;
  bool isNotificationOn; // does the client want to receive notifications?

  Client({
    required this.id,
    required this.fullName,
    required this.image,
    required this.email,
    required this.mobile,
    required this.fullMobile,
    required this.gender,
    required this.countryCode,
    required this.registrationDate,
    required this.isVerified,
    required this.countryId,
    required this.rating,
    required this.walletAmount,
    required this.isNotificationOn,
  });
}
