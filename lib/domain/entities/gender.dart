enum Gender {
  male(value: "male", id: 1, key: "M"),
  female(value: "female", id: 2, key: "F"),
  notSelected(value: "not selected", id: 0, key: "-");

  const Gender({required String value, required int id, required String key});
}