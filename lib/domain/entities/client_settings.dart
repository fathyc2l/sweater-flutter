import 'package:sweater_flutter/core/app_constants.dart';
import 'package:sweater_flutter/domain/entities/location_address.dart';
import 'package:uuid/uuid.dart';

class ClientSettings {
  String language;
  String? fcmToken;
  String? authToken;
  String deviceId;
  String? vatId;

  LocationAddress? savedLocation;

  bool isLoggedIn;
  bool isFcmTokenSentToBackend;
  bool isFirstOpen;
  bool isOriginalSourceSet;
  bool hasOldBooking;

  double vatPercentage;

  int? randomGiftAmount;
  int pendingTime;

  ClientSettings(
      {required this.language,
      required this.fcmToken,
      required this.authToken,
      required this.deviceId,
      required this.vatId,
      required this.savedLocation,
      required this.isLoggedIn,
      required this.isFcmTokenSentToBackend,
      required this.isFirstOpen,
      required this.isOriginalSourceSet,
      required this.hasOldBooking,
      required this.vatPercentage,
      required this.randomGiftAmount,
      required this.pendingTime});

  ClientSettings.defaultValue()
      : this(
            language: AppConstants.english,
            // TODO check device locale
            fcmToken: null,
            authToken: null,
            deviceId: const Uuid().v4(),
            vatId: null,
            savedLocation: null,
            isLoggedIn: false,
            isFcmTokenSentToBackend: false,
            isFirstOpen: true,
            isOriginalSourceSet: false,
            hasOldBooking: true,
            vatPercentage: 15.0,
            randomGiftAmount: null,
            pendingTime: 20);

  void logout() {
    fcmToken = null;
    authToken = null;
    isLoggedIn = false;
    isFcmTokenSentToBackend = false;
    savedLocation = null;
  }
}
