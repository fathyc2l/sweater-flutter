class LocationAddress {
  final int id;
  final double latitude;
  final double longitude;
  final String address;
  final String note;
  final LocationAddressType type;

  LocationAddress(
      {required this.id,
      required this.latitude,
      required this.longitude,
      required this.address,
      required this.note,
      required this.type});
}

enum LocationAddressType {
  home(id: 1),
  work(id: 2),
  other(id: 0);

  const LocationAddressType({required this.id});

  final int id;

  static LocationAddressType fromValue(int? id) {
    if (id == 0) {
      return LocationAddressType.other;
    } else if (id == 1) {
      return LocationAddressType.home;
    } else if (id == 2) {
      return LocationAddressType.work;
    } else {
      return LocationAddressType.other;
    }
  }
}
