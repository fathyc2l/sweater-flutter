import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:sweater_flutter/core/base/base_request_body.dart';
import 'package:sweater_flutter/core/errors/app_error.dart';
import 'package:sweater_flutter/data/models/request_body/create_order_request_body.dart';
import 'package:sweater_flutter/data/network/sweater_network.dart';
import 'package:sweater_flutter/core/payment/payment_order.dart';
import 'package:sweater_flutter/domain/repositories/orders_repository.dart';

class OrdersRepositoryImpl extends OrdersRepository {
  final SweaterNetwork _network;

  OrdersRepositoryImpl(this._network);

  @override
  Future<Either<AppError, PaymentOrder>> createPaymentOrder(
      {required BaseRequestBody body}) async {
    try {
      final response =
          await _network.createNewOrder(body as CreateOrderRequestBody);

      if (response.code == 200) {
        final data = response.data;
        return data == null
            ? Left(GeneralError('No Data!'))
            : Right(data.toEntity());
      }

      return Left(GeneralError("${response.code}: ${response.message}"));
    } on DioException catch (e) {
      return Left(GeneralError(getNetworkError(e)));
    } on Exception catch (e) {
      return Left(GeneralError(e.toString()));
    }
  }
}
