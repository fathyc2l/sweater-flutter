import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:sweater_flutter/core/base/base_request_body.dart';
import 'package:sweater_flutter/core/errors/app_error.dart';
import 'package:sweater_flutter/data/models/request_body/signin_request_body.dart';
import 'package:sweater_flutter/data/network/sweater_network.dart';
import 'package:sweater_flutter/domain/repositories/auth_repository.dart';

class AuthRepositoryImpl extends AuthRepository {
  final SweaterNetwork _network;

  AuthRepositoryImpl(this._network);

  @override
  Future<Either<AppError, bool>> signIn({required BaseRequestBody body}) async {
    try {
      final response = await _network.signIn(body as SignInRequestBody);

      if (response.code == 203) {
        return const Right(true);
      }

      return Left(GeneralError("${response.code}: ${response.message}"));
    } on DioException catch (e) {
      return Left(GeneralError(getNetworkError(e)));
    } on Exception catch (e) {
      return Left(GeneralError(e.toString()));
    }
  }
}
