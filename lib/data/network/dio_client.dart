import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:sweater_flutter/core/app_config.dart';
import 'package:sweater_flutter/core/sharedpreferences/shared_preferences_util.dart';

Dio buildDioClient(SharedPreferencesUtil sharedPreferencesUtil) {
  final dio = Dio()
    ..options = BaseOptions(baseUrl: AppConfig.config.flavor.baseUrl);

  dio.interceptors.addAll([
    HeadersInterceptor(sharedPreferencesUtil),
    PrettyDioLogger(requestHeader: true, requestBody: true, compact: false)
  ]);

  return dio;
}

class HeadersInterceptor extends Interceptor {
  final SharedPreferencesUtil sharedPreferencesUtil;

  HeadersInterceptor(this.sharedPreferencesUtil);

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    final clientSettings = sharedPreferencesUtil.getClientSettings();

    options.headers.addAll({
      "authorization": "Bearer ${clientSettings.authToken}",
      "lang": clientSettings.language,
      "Accept": "application/json",
      "Content-Type": "application/json",
    });
    handler.next(options);
  }
}
