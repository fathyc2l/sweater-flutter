// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_success_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ApiSuccessResponse<T> _$ApiSuccessResponseFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    ApiSuccessResponse<T>(
      data: _$nullableGenericFromJson(json['data'], fromJsonT),
      code: json['code'] as int?,
      message: json['message'] as String?,
    );

Map<String, dynamic> _$ApiSuccessResponseToJson<T>(
  ApiSuccessResponse<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': _$nullableGenericToJson(instance.data, toJsonT),
    };

T? _$nullableGenericFromJson<T>(
  Object? input,
  T Function(Object? json) fromJson,
) =>
    input == null ? null : fromJson(input);

Object? _$nullableGenericToJson<T>(
  T? input,
  Object? Function(T value) toJson,
) =>
    input == null ? null : toJson(input);
