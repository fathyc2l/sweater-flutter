import 'package:json_annotation/json_annotation.dart';

part 'api_success_response.g.dart';

@JsonSerializable(genericArgumentFactories: true, explicitToJson: true)
class ApiSuccessResponse<T> {
  final String? message;
  final int? code;
  final T? data;

  ApiSuccessResponse(
      {required this.data, required this.code, required this.message});

  factory ApiSuccessResponse.fromJson(
    Map<String, dynamic> json,
    T Function(Object? json) fromJsonT,
  ) =>
      _$ApiSuccessResponseFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object Function(T value) toJsonT) =>
      _$ApiSuccessResponseToJson(this, toJsonT);
}
