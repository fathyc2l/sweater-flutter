import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:sweater_flutter/data/models/request_body/create_order_request_body.dart';
import 'package:sweater_flutter/data/models/request_body/signin_request_body.dart';
import 'package:sweater_flutter/data/models/request_response/create_new_order_response.dart';

import 'api_success_response.dart';

part 'sweater_network.g.dart';

@RestApi()
abstract class SweaterNetwork {
  factory SweaterNetwork(Dio dio, {String baseUrl}) = _SweaterNetwork;

  // Authentication
  @POST('client_auth')
  Future<ApiSuccessResponse> signIn(@Body() SignInRequestBody body);

  @POST('create_new_order')
  Future<ApiSuccessResponse<CreateNewOrderResponse>> createNewOrder(@Body() CreateOrderRequestBody body);
}
