import 'package:json_annotation/json_annotation.dart';
import 'package:sweater_flutter/core/payment/payment_gateway.dart';
import 'package:sweater_flutter/core/payment/payment_order.dart';

part 'create_new_order_response.g.dart';

@JsonSerializable()
class CreateNewOrderResponse {
  @JsonKey(name: "transaction_id")
  final String transactionId;
  @JsonKey(name: "amount")
  final double amount;
  @JsonKey(name: "payment_method_value")
  final int paymentMethodValue;
  @JsonKey(name: "payment_method_text")
  final String paymentMethodText;
  @JsonKey(name: "webhook_url")
  final String webhookUrl;

  CreateNewOrderResponse(this.transactionId, this.amount,
      this.paymentMethodValue, this.paymentMethodText, this.webhookUrl);

  factory CreateNewOrderResponse.fromJson(Map<String, dynamic> json) =>
      _$CreateNewOrderResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CreateNewOrderResponseToJson(this);

  PaymentOrder toEntity() {
    return PaymentOrder(
      transactionId: transactionId,
      amount: amount,
      paymentGateWay: PaymentGateWay.fromValue(paymentMethodValue),
      redirectUrl: webhookUrl,
    );
  }
}
