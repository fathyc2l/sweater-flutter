// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_new_order_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateNewOrderResponse _$CreateNewOrderResponseFromJson(
        Map<String, dynamic> json) =>
    CreateNewOrderResponse(
      json['transaction_id'] as String,
      (json['amount'] as num).toDouble(),
      json['payment_method_value'] as int,
      json['payment_method_text'] as String,
      json['webhook_url'] as String,
    );

Map<String, dynamic> _$CreateNewOrderResponseToJson(
        CreateNewOrderResponse instance) =>
    <String, dynamic>{
      'transaction_id': instance.transactionId,
      'amount': instance.amount,
      'payment_method_value': instance.paymentMethodValue,
      'payment_method_text': instance.paymentMethodText,
      'webhook_url': instance.webhookUrl,
    };
