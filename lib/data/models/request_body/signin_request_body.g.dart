// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signin_request_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignInRequestBody _$SignInRequestBodyFromJson(Map<String, dynamic> json) =>
    SignInRequestBody(
      mobile: json['mobile'] as String?,
      deviceType: json['device_type'] as String,
      language: json['language'] as String,
      countryId: json['country_id'] as int,
    );

Map<String, dynamic> _$SignInRequestBodyToJson(SignInRequestBody instance) =>
    <String, dynamic>{
      'mobile': instance.mobile,
      'country_id': instance.countryId,
      'device_type': instance.deviceType,
      'language': instance.language,
    };
