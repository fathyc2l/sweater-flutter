import 'package:json_annotation/json_annotation.dart';
import 'package:sweater_flutter/core/base/base_request_body.dart';
import 'package:sweater_flutter/core/errors/validation_error.dart';

part 'create_order_request_body.g.dart';

@JsonSerializable()
class CreateOrderRequestBody extends BaseRequestBody {
  @JsonKey(name: "order_type")
  final int orderType;

  @JsonKey(name: "booking_id")
  int? bookingId;
  @JsonKey(name: "subscription_id")
  int? subscriptionId;
  @JsonKey(name: "user_subscription_id")
  int? userSubscriptionId;
  @JsonKey(name: "useWallet")
  int? useWallet;

  @JsonKey(name: "services_id")
  List<int>? addNewServicesIds;
  @JsonKey(name: "service_ids")
  List<int>? servicesIds;
  @JsonKey(name: "segment_services_ids")
  List<int>? segmentServicesIds;

  @JsonKey(name: "notes")
  String? notes;
  @JsonKey(name: "buildingName")
  String? buildingName;
  @JsonKey(name: "floorNumber")
  String? floorNumber;
  @JsonKey(name: "parkingNumber")
  String? parkingNumber;
  @JsonKey(name: "pillarNumber")
  String? pillarNumber;
  @JsonKey(name: "mobile")
  String? mobile;

  @JsonKey(name: "amount")
  double? amount;
  @JsonKey(name: "rating")
  double? rating;

  CreateOrderRequestBody({
    required this.orderType,
    this.bookingId,
    this.subscriptionId,
    this.userSubscriptionId,
    this.useWallet,
    this.addNewServicesIds,
    this.servicesIds,
    this.segmentServicesIds,
    this.notes,
    this.buildingName,
    this.floorNumber,
    this.parkingNumber,
    this.pillarNumber,
    this.mobile,
    this.amount,
    this.rating,
  });

  factory CreateOrderRequestBody.fromJson(Map<String, dynamic> json) =>
      _$CreateOrderRequestBodyFromJson(json);

  Map<String, dynamic> toJson() => _$CreateOrderRequestBodyToJson(this);

  @override
  ValidationError? validate() {
    return null;
  }
}
