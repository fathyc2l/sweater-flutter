// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_order_request_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateOrderRequestBody _$CreateOrderRequestBodyFromJson(
        Map<String, dynamic> json) =>
    CreateOrderRequestBody(
      orderType: json['order_type'] as int,
      bookingId: json['booking_id'] as int?,
      subscriptionId: json['subscription_id'] as int?,
      userSubscriptionId: json['user_subscription_id'] as int?,
      useWallet: json['useWallet'] as int?,
      addNewServicesIds: (json['services_id'] as List<dynamic>?)
          ?.map((e) => e as int)
          .toList(),
      servicesIds: (json['service_ids'] as List<dynamic>?)
          ?.map((e) => e as int)
          .toList(),
      segmentServicesIds: (json['segment_services_ids'] as List<dynamic>?)
          ?.map((e) => e as int)
          .toList(),
      notes: json['notes'] as String?,
      buildingName: json['buildingName'] as String?,
      floorNumber: json['floorNumber'] as String?,
      parkingNumber: json['parkingNumber'] as String?,
      pillarNumber: json['pillarNumber'] as String?,
      mobile: json['mobile'] as String?,
      amount: (json['amount'] as num?)?.toDouble(),
      rating: (json['rating'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$CreateOrderRequestBodyToJson(
        CreateOrderRequestBody instance) =>
    <String, dynamic>{
      'order_type': instance.orderType,
      'booking_id': instance.bookingId,
      'subscription_id': instance.subscriptionId,
      'user_subscription_id': instance.userSubscriptionId,
      'useWallet': instance.useWallet,
      'services_id': instance.addNewServicesIds,
      'service_ids': instance.servicesIds,
      'segment_services_ids': instance.segmentServicesIds,
      'notes': instance.notes,
      'buildingName': instance.buildingName,
      'floorNumber': instance.floorNumber,
      'parkingNumber': instance.parkingNumber,
      'pillarNumber': instance.pillarNumber,
      'mobile': instance.mobile,
      'amount': instance.amount,
      'rating': instance.rating,
    };
