import 'package:json_annotation/json_annotation.dart';
import 'package:sweater_flutter/core/base/base_request_body.dart';
import 'package:sweater_flutter/core/errors/validation_error.dart';
import 'package:sweater_flutter/core/extensions/string_extensions.dart';

part 'signin_request_body.g.dart';

// {"mobile":"0500000074","country_id":"2","device_type":"android","language":"en"}

@JsonSerializable()
class SignInRequestBody extends BaseRequestBody {
  final String? mobile;
  @JsonKey(name: "country_id")
  final int countryId;
  @JsonKey(name: "device_type")
  final String deviceType;
  final String language;

  SignInRequestBody(
      {required this.mobile,
      required this.deviceType,
      required this.language,
      required this.countryId});

  @override
  ValidationError? validate() {
    if (!mobile.isMobile()) {
      return MobileNotValid();
    }

    return null;
  }

  factory SignInRequestBody.fromJson(Map<String, dynamic> json) =>
      _$SignInRequestBodyFromJson(json);

  Map<String, dynamic> toJson() => _$SignInRequestBodyToJson(this);
}
