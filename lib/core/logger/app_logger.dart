import 'package:logger/logger.dart';

class AppLogger {
  static final _logger = Logger();

  static void error({required String tag, required String? error}) {
    _logger.e('$tag: $error');
  }

  static void debug({required String tag, required String? message}) {
    _logger.d('$tag: $message');
  }

  static void info({required String tag, required String? message}) {
    _logger.i('$tag: $message');
  }
}
