import 'package:dio/dio.dart';
import 'dart:convert';

import 'package:sweater_flutter/core/errors/network_error.dart';

abstract class BaseRepository {
  String? getNetworkError(DioException dioException) {
    return NetworkError.fromJson(json.decode(dioException.response.toString()))
        .toString();
  }
}
