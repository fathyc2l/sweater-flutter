import 'package:sweater_flutter/core/errors/validation_error.dart';

abstract class BaseRequestBody {
  ValidationError? validate();
}
