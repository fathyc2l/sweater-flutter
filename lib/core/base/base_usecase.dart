
import 'package:sweater_flutter/core/base/base_request_body.dart';

abstract class BaseUseCae<T> {
  Future<T> call({required BaseRequestBody body});
}
