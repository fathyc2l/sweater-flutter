enum PaymentCardBrand {
  masterCard("MASTERCARD", 3),
  visa("VISA", 2),
  amex("AMEX", 2),
  mada("MADA", 1),
  meeza("MEEZA", 2);

  const PaymentCardBrand(this.stringValue, this.intValue);

  final String stringValue;
  final int intValue;
}
