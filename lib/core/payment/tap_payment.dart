import 'package:go_sell_sdk_flutter/go_sell_sdk_flutter.dart';
import 'package:go_sell_sdk_flutter/model/models.dart';
import 'package:sweater_flutter/core/app_config.dart';
import 'package:sweater_flutter/core/app_constants.dart';
import 'package:sweater_flutter/core/extensions/string_extensions.dart';
import 'package:sweater_flutter/core/payment/payment_gateway.dart';
import 'package:sweater_flutter/core/payment/payment_requrest.dart';
import 'package:sweater_flutter/core/payment/payment_result.dart';
import 'package:sweater_flutter/core/payment/payment_status_callback.dart';
import 'package:sweater_flutter/core/sharedpreferences/shared_preferences_util.dart';

class TapPayment {
  final SharedPreferencesUtil _sharedPreferencesUtil;

  TapPayment(this._sharedPreferencesUtil) {
    _configureSdk();
  }

  void _configureSdk() {
    GoSellSdkFlutter.configureApp(
        productionSecretKey: AppConfig.config.keysConfig.tapPaymentLiveKey,
        sandBoxSecretKey: AppConfig.config.keysConfig.tapPaymentTestKey,
        bundleId: AppConfig.config.keysConfig.tapPaymentBundleId,
        lang: _sharedPreferencesUtil.getClientSettings().language);
  }

  Future<void> initPayment({
    required PaymentRequest paymentRequest,
    required PaymentStatusCallback paymentStatusCallback,
  }) async {
    final client = _sharedPreferencesUtil.getClientData();
    if (client == null) {
      paymentStatusCallback.onPaymentFailed(error: "CLIENT_IS_NULL");
      return;
    }

    final customer = Customer(
        isdNumber: AppConstants.defaultCountryCode,
        number: client.mobile,
        customerId: client.id.toString(),
        email: client.email,
        firstName: client.fullName,
        middleName: "",
        lastName: "");

    GoSellSdkFlutter.sessionConfigurations(
        trxMode: TransactionMode.PURCHASE,
        transactionCurrency: AppConstants.defaultCurrency,
        amount: paymentRequest.amount.toString(),
        customer: customer,
        paymentItems: List.empty(),
        taxes: List.empty(),
        shippings: List.empty(),
        postURL: paymentRequest.redirectUrl,
        paymentDescription: paymentRequest.description,
        paymentMetaData: {'device_type': AppConstants.deviceType()},
        paymentReference: Reference(
          transaction: paymentRequest.transactionId,
          order: paymentRequest.transactionId,
        ),
        paymentStatementDescriptor: "paymentStatementDescriptor",
        isUserAllowedToSaveCard: false,
        isRequires3DSecure: true,
        receipt: Receipt(true, false),
        authorizeAction:
            AuthorizeAction(type: AuthorizeActionType.CAPTURE, timeInHours: 10),
        // TODO ask about timeInHours
        merchantID: "merchantID",
        // TODO ask about merchantID
        allowedCadTypes: CardType.ALL,
        sdkMode:
            AppConfig.isProduction() ? SDKMode.Production : SDKMode.Sandbox,
        paymentType: PaymentType.ALL,
        allowsToSaveSameCardMoreThanOnce: true,
        cardHolderName: "",
        destinations: null,
        applePayMerchantID: "applePayMerchantID",
        // TODO ask about applePayMerchantID
        allowsToEditCardHolderName: true);

    final result = await GoSellSdkFlutter.startPaymentSDK;

    switch (result['sdk_result']) {
      case "SUCCESS":
        paymentStatusCallback.onPaymentSuccess(
            result: PaymentResult(
                orderId: result['charge_id'],
                transactionId: paymentRequest.transactionId,
                chargeId: result['charge_id'],
                amountPaid: paymentRequest.amount,
                cardBrand: '${result['card_brand']}'.toCardBrand(),
                paidWith: PaymentGateWay.tap,
                firstSixDigitsOfPaymentCard: result['card_first_six']));
        break;
      case "FAILED":
        paymentStatusCallback.onPaymentFailed(error: 'FAILED');
        break;
      case "SDK_ERROR":
        paymentStatusCallback.onPaymentFailed(
            error:
                '${result['sdk_error_code']}: ${result['sdk_error_message']}');
        break;
      case "NOT_IMPLEMENTED":
        paymentStatusCallback.onPaymentFailed(error: 'NOT_IMPLEMENTED');
        break;
    }
  }
}
