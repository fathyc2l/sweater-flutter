enum PaymentGateWay {
  tap(3),
  payFort(2),
  tabby(null); // Tabby not supported yet for orders

  final int? intValue;

  const PaymentGateWay(this.intValue);

  static PaymentGateWay fromValue(int? id) {
    if (id == 2) {
      return PaymentGateWay.payFort;
    } else if (id == 3) {
      return PaymentGateWay.tap;
    } else {
      return PaymentGateWay.tap;
    }
  }
}
