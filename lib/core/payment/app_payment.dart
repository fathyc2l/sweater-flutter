import 'package:sweater_flutter/core/payment/payment_gateway.dart';
import 'package:sweater_flutter/core/payment/payment_requrest.dart';
import 'package:sweater_flutter/core/payment/payment_status_callback.dart';
import 'package:sweater_flutter/core/payment/tap_payment.dart';
import 'package:sweater_flutter/data/models/request_body/create_order_request_body.dart';
import 'package:sweater_flutter/domain/usecases/create_payment_order_usecase.dart';

abstract class AppPayment {
  Future<void> purchase(CreateOrderRequestBody newOrderRequestBody,
      PaymentStatusCallback paymentStatusCallback);
}

class AppPaymentImpl extends AppPayment {
  final CreatePaymentOrderUseCase _createPaymentOrderUseCase;
  final TapPayment _tapPayment;

  AppPaymentImpl(this._createPaymentOrderUseCase, this._tapPayment);

  @override
  Future<void> purchase(CreateOrderRequestBody newOrderRequestBody,
      PaymentStatusCallback paymentStatusCallback) async {
    final result = await _createPaymentOrderUseCase.call(body: newOrderRequestBody);

    result.fold((error) {
      paymentStatusCallback.onPaymentFailed(error: error.toErrorString());
    }, (activeOrder) {
      final paymentRequest = PaymentRequest(
        amount: activeOrder.amount,
        redirectUrl: activeOrder.redirectUrl,
        transactionId: activeOrder.redirectUrl,
      );

      if (activeOrder.paymentGateWay == PaymentGateWay.tap) {
        _tapPayment.initPayment(
          paymentRequest: paymentRequest,
          paymentStatusCallback: paymentStatusCallback,
        );
      } else {
        // TODO other supported payment gateways goes here
        paymentStatusCallback.onPaymentFailed(
            error: "Unsupported payment gateway!");
      }
    });
  }
}
