import 'package:sweater_flutter/core/payment/payment_gateway.dart';

import 'payment_card_brand.dart';

class PaymentResult {
  final String orderId;
  final String transactionId;
  final String chargeId;
  final String? firstSixDigitsOfPaymentCard;
  final double amountPaid;
  final PaymentCardBrand cardBrand;
  final PaymentGateWay paidWith;

  PaymentResult({
    required this.orderId,
    required this.transactionId,
    required this.chargeId,
    required this.amountPaid,
    required this.cardBrand,
    required this.paidWith,
    this.firstSixDigitsOfPaymentCard,
  });
}
