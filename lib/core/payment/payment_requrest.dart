class PaymentRequest {
  final double amount;
  final String redirectUrl;
  final String transactionId;
  final String description;

  PaymentRequest(
      {required this.amount,
      required this.redirectUrl,
      required this.transactionId,
      this.description = ""});
}
