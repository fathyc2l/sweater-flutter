import 'package:sweater_flutter/core/payment/payment_result.dart';

abstract class PaymentStatusCallback {
  void onPaymentSuccess({required PaymentResult result});

  void onPaymentFailed({required String error});
}
