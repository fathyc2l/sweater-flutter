import 'package:sweater_flutter/core/payment/payment_gateway.dart';

class PaymentOrder {
  final String transactionId;
  final double amount;
  final PaymentGateWay paymentGateWay;
  final String redirectUrl;

  PaymentOrder({
    required this.transactionId,
    required this.amount,
    required this.paymentGateWay,
    required this.redirectUrl,
  });
}
