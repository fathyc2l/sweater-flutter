import 'dart:io';

enum Flavor {
  staging(baseUrl: "https://kotr.app/api/v1/"),
  newInfra(baseUrl: "https://staging.sweater.sa/api/v1/"),
  production(baseUrl: "https://app.sweater.sa/api/v1/");

  final String baseUrl;

  const Flavor({required this.baseUrl});
}

class AppConfig {
  Flavor flavor = Flavor.staging;

  final KeysConfig keysConfig =
      Platform.isAndroid ? AndroidKeysConfig() : IOSKeysConfig();

  AppConfig(this.flavor);

  static AppConfig config = AppConfig.create();

  static bool isProduction() {
    return config.flavor == Flavor.production;
  }

  factory AppConfig.create({Flavor flavor = Flavor.staging}) {
    return config = AppConfig(flavor);
  }
}

abstract class KeysConfig {
  abstract String tapPaymentBundleId;
  abstract String tapPaymentTestKey;
  abstract String tapPaymentLiveKey;
}

class AndroidKeysConfig extends KeysConfig {
  @override
  String tapPaymentBundleId = "com.octalsoftaware.sweater";

  @override
  String tapPaymentLiveKey = "sk_live_vJ1A2xbKtDsgXhEZWNklPwmR";

  @override
  String tapPaymentTestKey = "sk_test_k3KPE6yIfGLSmrjHqA1shOVQ";
}

// TODO add right keys
class IOSKeysConfig extends KeysConfig {
  @override
  String tapPaymentBundleId = "";

  @override
  String tapPaymentLiveKey = "";

  @override
  String tapPaymentTestKey = "";
}
