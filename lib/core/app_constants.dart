import 'dart:io';

abstract class AppConstants {
  static const int defaultCountryId = 2;
  static const String defaultCountryCode = "+966";
  static const String defaultCurrency = "SAR";

  static String deviceType() {
    if (Platform.isAndroid) {
      return "android";
    } else {
      return "ios";
    }
  }

  static const String arabic = "ar";
  static const String english = "en";
}
