
abstract class AppError {
  String toErrorString();
}

class GeneralError extends AppError {
  final String? message;

  GeneralError(this.message);

  @override
  String toErrorString() {
    return message ?? "SomethingWentWrong";
  }
}