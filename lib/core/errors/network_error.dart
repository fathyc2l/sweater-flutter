import 'package:json_annotation/json_annotation.dart';

part 'network_error.g.dart';

@JsonSerializable()
class NetworkError {
  final bool? success;
  final int? code;
  final String? message;
  final List<String>? errors;

  NetworkError(this.success, this.code, this.message, this.errors);

  factory NetworkError.fromJson(Map<String, dynamic> json) =>
      _$NetworkErrorFromJson(json);

  Map<String, dynamic> toJson() => _$NetworkErrorToJson(this);

  @override
  String toString() {
    if (errors == null || (errors != null && errors!.isEmpty)) {
      return message ?? "$code";
    }

    String errorString = '';
    for (int i = 0; i < errors!.length; i++) {
      errorString = errorString + errors![i].toString();
      if (i != errors!.length - 1) errorString = '$errorString\n';
    }

    return errorString;
  }
}