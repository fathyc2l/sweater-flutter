import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweater_flutter/core/extensions/object_extensions.dart';
import 'package:sweater_flutter/core/extensions/string_extensions.dart';
import 'package:sweater_flutter/core/sharedpreferences/shared_preferences_util.dart';
import 'package:sweater_flutter/domain/entities/client.dart';
import 'package:sweater_flutter/domain/entities/client_settings.dart';

class SharedPreferencesUtilImpl extends SharedPreferencesUtil {
  final _clientKey = "_preferences_client_key";
  final _clientSettingsKey = "_preferences_client_settings_key";

  final SharedPreferences preferences;

  SharedPreferencesUtilImpl(this.preferences);

  @override
  void saveClientData(Client client) async {
    await preferences.setString(_clientKey, client.toJsonString());
  }

  @override
  Client? getClientData() {
    final jsonString = preferences.getString(_clientKey);

    if (jsonString.isNullOrEmpty()) {
      return null;
    }

    try {
      return jsonDecode(jsonString!);
    } on Exception {
      return null;
    }
  }

  @override
  void saveClientSettings(ClientSettings settings) async {
    await preferences.setString(_clientSettingsKey, settings.toJsonString());
  }

  @override
  ClientSettings getClientSettings() {
    final jsonString = preferences.getString(_clientSettingsKey);

    if (jsonString.isNullOrEmpty()) {
      return ClientSettings.defaultValue();
    }

    try {
      return jsonDecode(jsonString!);
    } on Exception {
      return ClientSettings.defaultValue();
    }
  }

  @override
  void logout() async {
    var settings = getClientSettings();

    await preferences.remove(_clientKey);

    settings.logout();
    saveClientSettings(settings);

    //  TODO delete current firebase messaging token
  }
}
