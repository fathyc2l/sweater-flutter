import 'package:sweater_flutter/domain/entities/client.dart';
import 'package:sweater_flutter/domain/entities/client_settings.dart';

abstract class SharedPreferencesUtil {
  void saveClientData(Client client);
  Client? getClientData();

  void saveClientSettings(ClientSettings settings);
  ClientSettings getClientSettings();

  void logout();
}
