import 'package:sweater_flutter/core/payment/payment_card_brand.dart';
import 'package:sweater_flutter/domain/entities/gender.dart';

extension StringExtensions on String? {
  bool isNullOrEmpty() {
    return this == null || this?.isEmpty == true;
  }

  bool isMobile() {
    return !isNullOrEmpty() && RegExp(r"^[5]\\d{8}$").hasMatch(this ?? "");
  }

  bool isPassword() {
    return !isNullOrEmpty() && (this?.length ?? 0) >= 6;
  }

  Gender toGender() {
    if (this == "male") {
      return Gender.male;
    } else if (this == "female") {
      return Gender.female;
    } else {
      return Gender.notSelected;
    }
  }

  PaymentCardBrand toCardBrand() {
    if (isNullOrEmpty()) {
      return PaymentCardBrand.visa;
    }

    return PaymentCardBrand.values.firstWhere(
        (element) => element.name.toLowerCase() == this!.toLowerCase(), orElse: () => PaymentCardBrand.visa );
  }
}
