import 'dart:convert';

extension StringExtensions on Object?{
  String toJsonString() {
    if(this == null) {
      return "null";
    }

    try {
      return jsonEncode(this);
    } on Exception catch (e) {
      return e.toString();
    }
  }
}