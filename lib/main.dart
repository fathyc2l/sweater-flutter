import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:sweater_flutter/app/designsystem/app_theme.dart';
import 'package:sweater_flutter/app/screens/signin/sign_in.dart';
import 'package:sweater_flutter/core/app_config.dart';
import 'package:sweater_flutter/di/injection.dart';
import 'package:sweater_flutter/l10n/app_localizations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies();
  AppConfig.create(flavor: Flavor.staging);
  runApp(const SweaterApp());
}

class SweaterApp extends StatelessWidget {
  const SweaterApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      minTextAdapt: true,
      splitScreenMode: true,
      child: const SignIn(),
      builder: (_, child) {
        return MaterialApp(
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: AppLocalizations.supportedLocales,
          theme: ThemeData(
            colorScheme: AppTheme.colorScheme,
            useMaterial3: true,
          ),
          home: child,
        );
      },
    );
  }
}
