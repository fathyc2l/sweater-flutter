import 'package:flutter/material.dart';
import 'package:sweater_flutter/app/designsystem/widgets/app_image.dart';
import 'package:sweater_flutter/gen/assets.gen.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          AppVectorImageWidget(imagePath: Assets.images.icSiginLogo)
        ],
      ),
    );
  }
}
