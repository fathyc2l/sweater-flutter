import 'package:flutter/material.dart';

abstract class AppTheme {
  static const primaryColor = Color(0xFFEF5B0C);
  static const accentColor = Color(0xFFEF5B0C);

  static final colorScheme = ColorScheme.fromSeed(
      seedColor: primaryColor, brightness: Brightness.light);
}
