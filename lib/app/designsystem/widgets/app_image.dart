
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppVectorImageWidget extends StatelessWidget {
  final String _imageAssetPath;

  const AppVectorImageWidget({required String imagePath, super.key}) : _imageAssetPath = imagePath;

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(_imageAssetPath);
  }
}
